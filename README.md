## What is this
This is a client for uploading files coupled with the file upload server part.
The client expects a directory as input and traces all the files in it.
It tries to upload each of them. If the server returns Too Many Connections code,
the client sleeps for a while and continues after this. At the end it deletes 
the uploaded file. The client sends a special "sleep" header to the server
instructing it to sleep for 5 seconds during each file upload to simulate
heavy loads from big files.

## How to start it
We expect a directory and serverUrl as input

```bash
mvn exec:java -Ddirectory=/home/dido/input -DserverUrl=http://localhost:8080
``` 

## How to use it
Nothing special. You just start it and it does its job as described in the first part.