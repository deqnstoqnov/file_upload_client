package com.file.upload.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.Deflater;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

/**
 * Class that uploads a single file. We share a single http client instance for
 * all single file upload instances. Each file upload will take 5 seconds.
 * 
 * @author dido
 *
 */
public class SingleFileUploadClient {

	private String serverUrl;
	private String fileUrl;
	private static final HttpClient client = HttpClientBuilder.create().build();

	/**
	 * 
	 * @param serverUrl The server url plus the port
	 * @param fileUrl   The fullpath to the file to be uploaded.
	 */
	public SingleFileUploadClient(String serverUrl, String fileUrl) {
		this.serverUrl = serverUrl;
		this.fileUrl = fileUrl;

	}

	/**
	 * 
	 * @return The http status code that the server returns
	 * @throws IOException
	 */
	public int upload() throws IOException {
		@SuppressWarnings("deprecation")
		final HashCode hash = com.google.common.io.Files.hash(new File(fileUrl), Hashing.md5());
		final String md5 = hash.toString().toLowerCase();
		final String url = serverUrl + "/app/files/" + md5;

		final HttpPut put = new HttpPut(url);
		final byte[] encoded = Files.readAllBytes(Paths.get(fileUrl));
		put.setEntity(new ByteArrayEntity(encoded));
		put.setHeader("Content-Type", "application/octet-stream");
		put.setHeader("sleep", "5");

		final HttpResponse response = client.execute(put);
		return response.getStatusLine().getStatusCode();
	}

	public void deleteFile() throws IOException {
		Files.deleteIfExists(new File(fileUrl).toPath());
	}

}
