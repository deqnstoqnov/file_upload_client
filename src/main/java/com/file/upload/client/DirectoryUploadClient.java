package com.file.upload.client;

import java.io.IOException;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryUploadClient {

	private String directory;
	private String serverUrl;

	public DirectoryUploadClient(String directory, String serverUrl) {
		this.directory = directory;
		this.serverUrl = serverUrl;
	}

	public void trace() throws IOException {
		FileVisitor<Path> fileProcessor = new ProcessFile(serverUrl);
		Files.walkFileTree(Paths.get(directory), fileProcessor);
	}

}
