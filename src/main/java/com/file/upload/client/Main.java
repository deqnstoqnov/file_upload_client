package com.file.upload.client;

import java.io.IOException;

/**
 * We rely on the "directory" and the "serverUrl" system property
 * 
 * @author dido
 *
 */
public class Main {

	public static void main(String[] args) throws IOException {
		new DirectoryUploadClient(System.getProperty("directory"), System.getProperty("serverUrl")).trace();
	}
}
