package com.file.upload.client;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * A class that traces all the files in the given directory. Here we use an
 * instance of SingleFileUploadClient for each file we trace. If the server
 * returns Too Many Connections we take some rest and retry afterwards until
 * successful upload.
 * 
 * @author dido
 *
 */
class ProcessFile extends SimpleFileVisitor<Path> {

	private String serverUrl;

	public ProcessFile(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		SingleFileUploadClient client = new SingleFileUploadClient(serverUrl, file.toAbsolutePath().toString());
		while (client.upload() != 200) {
			takeSomeRest();
		}
		client.deleteFile();
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		System.out.println("Processing directory:" + dir);
		return FileVisitResult.CONTINUE;
	}

	private void takeSomeRest() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}